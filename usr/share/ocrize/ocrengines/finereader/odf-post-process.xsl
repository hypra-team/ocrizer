<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
  version="1.0">
  <xsl:output
    method="xml"
    indent="no"
    encoding="UTF-8"
    standalone="yes"
    />

<xsl:param name="page-width" select="'21cm'" />

<!-- styles -->
<xsl:template match="office:automatic-styles">
  <xsl:copy>
    <xsl:apply-templates select="@* | node()"/>
    <!-- create styles for each sections we create based on the corresponding
         frame properties -->
    <xsl:message>Using page-width=<xsl:value-of select="$page-width"/></xsl:message>
    <xsl:for-each select="//draw:frame[draw:text-box]">
      <xsl:element name="style:style">
        <xsl:attribute name="style:family">section</xsl:attribute>
        <xsl:attribute name="style:name">
          <xsl:value-of select="concat('PlaceholderSection', generate-id(.))"/>
        </xsl:attribute>
        <xsl:element name="style:section-properties">
          <xsl:attribute name="fo:margin-left"><xsl:value-of select="@svg:x"/></xsl:attribute>
          <xsl:attribute name="fo:margin-right"><xsl:value-of select="substring-before($page-width, 'cm') - substring-before(@svg:width, 'cm') - substring-before(@svg:x, 'cm')"/>cm</xsl:attribute>
        </xsl:element>
      </xsl:element>
    </xsl:for-each>
    <!-- create a derived style for each paragraph we strip from around a frame,
         which should be identical but for having a very small line height.
         It's not 0, otherwise if it's the style of the first paragraph in the
         document, arrowing up moves inside the header for some reason, so make
         it simply "very small" (0.001cm and above works, 0.0001cm doesn't).
         It's likely because under a certain size libreoffice considers it
         invisible, and this triggers a bug of some kind. -->
    <xsl:for-each select="//text:p[draw:frame]
                         |//text:h[draw:frame]">
      <xsl:element name="style:style">
        <xsl:variable name="style-name" select="@text:style-name"/>

        <xsl:attribute name="style:family">paragraph</xsl:attribute>
        <xsl:attribute name="style:name">
          <xsl:value-of select="concat('Placeholder', $style-name)"/>
        </xsl:attribute>
        <xsl:attribute name="style:parent-style-name">
          <xsl:value-of select="$style-name"/>
        </xsl:attribute>
        <!-- copy the original style:master-page-name attribute -->
        <xsl:copy-of select="//office:automatic-styles/style:style[@style:name=$style-name]/@style:master-page-name"/>

        <xsl:element name="style:paragraph-properties">
          <xsl:attribute name="fo:line-height">0.001cm</xsl:attribute>
        </xsl:element>
      </xsl:element>
    </xsl:for-each>
  </xsl:copy>
</xsl:template>

<!-- style: avoid images and text overlapping by modifying "run-through" wrap
     style with "dynamic" that does what we want. -->
<xsl:template match="office:automatic-styles/style:style/style:graphic-properties[@style:wrap='run-through']">
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <!-- override previous value -->
    <xsl:attribute name="style:wrap">dynamic</xsl:attribute>
    <xsl:apply-templates select="node()"/>
  </xsl:copy>
</xsl:template>

<!-- strip empty paragraphs that are likely here only to account for some
     frame sizes and will increase the page contents size for no good reason
     once the frames are stripped -->
<xsl:template match="text:p[count(*) = 0][count(preceding-sibling::*) > 0]">
  <xsl:comment>Stripped empty &lt;<xsl:value-of select="name()"/> text:style-name="<xsl:value-of select="@text:style-name"/>"&gt;</xsl:comment>
</xsl:template>

<!-- moves children of a draw:p containing a draw:frame outside of itself,
     because we want to replace the frame with sections, which do not belong
     inside a text:p -->
<xsl:template match="text:p[draw:frame/draw:text-box] |
                     text:h[draw:frame/draw:text-box]">
  <xsl:comment>Removed outer <xsl:value-of select="name()"/></xsl:comment>
  <!-- We keep an empty copy with an altered version of its style so the side
       effects of the style are applied. -->
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <xsl:for-each select="@text:style-name">
      <xsl:attribute name="{name(.)}">
        <xsl:value-of select="concat('Placeholder', .)"/>
      </xsl:attribute>
    </xsl:for-each>
  </xsl:copy>

  <xsl:variable name="self" select="."/>
  <!-- walk through the children and pack them inside their own wrapping
       element if appropriate. We wrap contiguous non-frame children together. -->
  <xsl:for-each select="*">
    <xsl:variable name="item-pos" select="count(preceding-sibling::*) + 1"/>
    <xsl:choose>
      <!-- child is a draw:frame we should strip -->
      <xsl:when test="self::draw:frame[draw:text-box]">
        <xsl:comment>draw:frame</xsl:comment>
        <xsl:comment>draw:text-box</xsl:comment>
        <xsl:element name="text:section">
          <xsl:attribute name="text:name">
            <xsl:value-of select="concat('PlaceholderSection', generate-id())" />
          </xsl:attribute>
          <xsl:attribute name="text:style-name">
            <xsl:value-of select="concat('PlaceholderSection', generate-id())" />
          </xsl:attribute>
          <xsl:apply-templates select="./draw:text-box/*"/>
        </xsl:element>
      </xsl:when>

      <!-- We group everything we can in one single extra element, to avoid
           creating many duplicate wrappers for sibling elements.  To do so,
           we match children that come right after a stripped frame, add the
           outer element back and loop on self and its following non-stripped
           elements to put them inside. -->
      <!-- TODO: maybe come up with a selector for the outer for-each that
                 avoids the need for matching here? -->
      <xsl:when test="$item-pos = 1 or ../draw:frame[$item-pos - 1][draw:text-box]">
        <xsl:comment>Re-added <xsl:value-of select="name($self)" /> wrapping element</xsl:comment>
        <!-- FIXME: there's probably a better way to do that.
             Here in order to change the context (and because XSLT 1.0 doesn't have
             the "select" attribute on xsl:copy), we use a xsl:for-each that matches
             a single element in order to switch context -->
        <xsl:for-each select="$self">
          <xsl:copy>
            <!-- We DO NOT copy the style attribute verbatim here, because it might
                 have extra side effects (even crash LO when using master-page-name
                 style attributes, hehe) -->
            <!-- TODO: add the stripped element's attributes here (?) -->
            <!-- copy the mandatory text:outline-level attribute of text:h elements -->
            <xsl:for-each select="@text:outline-level">
              <xsl:apply-templates select="."/>
            </xsl:for-each>

            <!-- copy all the non-stripped contiguous siblings -->
            <xsl:for-each select="./*[not(self::draw:frame[draw:text-box]) and
                                      count(preceding-sibling::*) + 1 >= $item-pos and
                                      not(preceding-sibling::draw:frame[count(preceding-sibling::*) + 1 > $item-pos][draw:text-box])]">
              <xsl:apply-templates select="."/>
            </xsl:for-each>
          </xsl:copy>
        </xsl:for-each>
      </xsl:when>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<!-- Detect frames we can't handle -->
<xsl:template match="draw:frame[count(draw:text-box) > 0 and count(draw:text-box) != count(*)]">
  <xsl:message>Unsupported input with a frame combining children of different types</xsl:message>
  <xsl:comment>Unsupported input with a frame combining children of different types</xsl:comment>
  <xsl:call-template name="copy-and-recurse"/>
</xsl:template>

<!-- Just catch cases we don't support yet but likely should, if any -->
<xsl:template match="*[not(self::text:p) and not(self::text:h)][draw:frame[count(draw:text-box) = count(*)]]">
  <xsl:message>Unsupported input with a draw:frame not inside a text:p or text:h (but a <xsl:value-of select="name()"/>) but with draw:text-box children</xsl:message>
  <xsl:comment>Unsupported input with a draw:frame not inside a text:p or text:h (but a <xsl:value-of select="name()"/>) but with draw:text-box children</xsl:comment>
  <xsl:call-template name="copy-and-recurse"/>
</xsl:template>

<!-- base template to match anything and call other templates -->
<xsl:template match="@* | node()" name="copy-and-recurse">
  <xsl:copy>
    <xsl:apply-templates select="@* | node()"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>
