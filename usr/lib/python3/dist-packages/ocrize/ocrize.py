#!/usr/bin/env python3

# Copyright 2017-2027 Auboyneau Vincent <ksamak@riseup.net>
# Copyright 2017 Hypra team <bugs@hypra.fr>
#
# This file is part of the OCRizer project.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

__author__ = "Ksamak"
__copyright__ = "Copyright 2017, Ksamak"
__credits__ = ["Hypra Sas"]
__license__ = "GPL"
__version__ = "3.0+"
__maintainer__ = "Alex ARNAUD"
__email__ = "bugs@hypra.fr"

import os
import subprocess
import sane
import _sane
import logging
import logging.handlers
import traceback
import argparse
import sys
import re
import shutil
import notify2
import tempfile
import gettext
import glob

class OCRizer():
    priority = -1
    def __init__(self, args):
        self.notifyer_first_run = True
        self.args = args
        self.init_logger()
        os.environ["PATH"] = "/opt/libreoffice4.2/program/" + os.pathsep + os.environ["PATH"]
        self.scan_file = ""
        self.ocrized_text = ""
        self.input_files = []
        self.output_files = []
        self.temp_files = []
        self.datadir = os.environ.get('OCRIZE_DATADIR', '/usr/share/ocrize')
        self.engine_datadir = os.path.join(self.datadir, 'ocrengines/%s' % self.engine_name)

    def init_logger(self):
        # list to convert verbose argument to the logging numeric level value
        verboseToLogging = [logging.CRITICAL, logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG]
        numeric_level = verboseToLogging[self.args.verbose - 1]
        logging.basicConfig(level=numeric_level)
        self.logger = logging.getLogger('Logger')
        if self.args.syslog:
            handler = logging.handlers.SysLogHandler(address='/dev/log')
            formatter = logging.Formatter('%(module)s:%(levelname)s: %(message)s')
            handler.setFormatter(formatter)
            self.logger.addHandler(handler)

    def send_notification(self, text):
        self.logger.info(text)
        if self.notifyer_first_run:
            self.notifyer_first_run = False
            self.display = os.getenv("DISPLAY")
            if self.display:
                self.notification = notify2.Notification("", "", "notification-message-im")
                self.notification.set_urgency(notify2.URGENCY_NORMAL)
                self.notification.set_timeout(10000) #set to 10 seconds.
        if self.display:
            notify2.init("ocrizer")
            self.notification.close()
            self.notification.update(text)
            self.notification.show()

    def get_option_by_name(self, scanner, name):
        options = scanner.get_options()
        for option in options:
            if option[1] == name:
                return option
        return False

    def get_max_resolution(self, scanner):
        option = self.get_option_by_name(scanner, "resolution")
        if option:
            return sorted(option[-1])[-1]
        return False

    def scan(self):
        if not self.args.scan_document:
            return

        self.send_notification(_("Attempting to scan a new image"))
        sane_version = sane.init()

        chosen_dev = self.select_scanner()
        self.logger.info(_('Chosen scanner : ' + str(chosen_dev)))

        dev = sane.open(chosen_dev[0])
        self.send_notification(_("Found %s as acquisition device" %chosen_dev[2]))

        try:
            dev.mode = 'color'
        except _sane.error:
            self.logger.info(_('Mode "color" not accepted by the driver'))
            pass

        max_resolution = self.get_max_resolution(dev)
        if max_resolution != False:
            if self.args.scan_resolution > max_resolution:
                dev.resolution = max_resolution
            else:
                dev.resolution = self.args.scan_resolution
            self.logger.info(_('Resolution : ' + str(dev.resolution)))

        params = dev.get_parameters()
        self.logger.info(_('Device parameters: ' + str(params)))

        self.send_notification(_("Scanning a document, please wait"))
        # get a PIL.Image object
        im = dev.scan()

        tmp_dir = tempfile._get_default_tempdir()
        tmp_file = next(tempfile._get_candidate_names()) + '.jpg'
        self.scan_file = os.path.join(tmp_dir, tmp_file)
        im.save(self.scan_file)
        self.temp_files.append(self.scan_file)
        self.save_to_pdf(self.scan_file)

        dev.close()
        self.args.files = [self.scan_file] # overrides other input files.

    def filter_scanner(self, devices):
        chosen_dev = None
        if self.args.chosen_scanner:
            for device in devices:
                if self.args.chosen_scanner == device[0]:
                    chosen_dev = device
                    break
        else:
            for device in devices:
                # take the first scanner found
                # virtual device are usallly webcams
                if 'virtual device' != device[3]:
                    chosen_dev = device
                    break

        return chosen_dev

    def select_scanner(self):
        self.list_scanners()

        # check local devices
        chosen_dev = self.filter_scanner(sane.get_devices(True))
        if chosen_dev == None:
            # check all devices
            devices = sane.get_devices()
            chosen_dev = self.filter_scanner(devices)

            if chosen_dev == None:
                if self.args.all_scanners:
                    try:
                        chosen_dev = devices[0]
                    except IndexError as e:
                        e = OcrizerException(_("Couldn't find any scanner device"))
                        e.notification_message = _("Couldn't find any scanner device")
                        raise e
                else:
                    e = OcrizerException(_("Couldn't find any scanner device"))
                    e.notification_message = _("Couldn't find any scanner device")
                    raise e

        return chosen_dev

    def list_scanners(self):
        if not self.args.list_scanners:
            return
        devices = sane.get_devices()
        for device in devices:
            print (device)
        sys.exit(0)

    def save_to_pdf(self, fil):
        filename, ext = os.path.splitext(fil)
        try:
            convert_function = getattr(self, "convert_%s_to_pdf" %ext.replace(".","").lower())
        except Exception as e:
            e.notification_message = _("Couldn't manage the input format %s" %ext.replace(".",""))
            raise e
        new_file = convert_function(fil)
        final_dest = self.get_next_save_name()
        cmd_args = ["mv", new_file, final_dest + ".pdf"]
        if self.args.verbose >= 3:
            print(" ".join(cmd_args))
        subprocess.check_call(cmd_args, stdout=subprocess.DEVNULL)

    def get_next_save_name(self):
        expended_output_dir = os.path.expanduser(self.args.output_dir)
        fil = ''
        file_list = [fil for fil in os.listdir(expended_output_dir) if fil.startswith(_("scanned_document_"))]
        file_list.sort()
        if len(file_list) != 0:
            next_num = int(os.path.splitext(file_list[-1].replace(_("scanned_document_"), ""))[0]) + 1
        else:
            next_num = 1
        return os.path.join(expended_output_dir, _("scanned_document_") + str(next_num).zfill(3))

    def convert_txt_to_odt(self, filename):
        temp_lo_user_dir = tempfile.mkdtemp()
        cmd_args = ["soffice", "-env:UserInstallation=file://" + temp_lo_user_dir, "--headless", "--convert-to", "odt", "--outdir", os.path.split(filename)[0], filename]
        if self.args.verbose >= 3:
            print(" ".join(cmd_args))
        try:
            res = subprocess.check_call(cmd_args, stdout=subprocess.DEVNULL)
        except Exception as e:
            e.notification_message = _("An internal error occurred")
            raise e
        finally:
            shutil.rmtree(temp_lo_user_dir, ignore_errors=True)
        self.temp_files.append(filename) # add for cleanup
        return re.sub("(?i)\.txt", ".odt", filename)

    def convert_txt_to_pdf(self, filename):
        return self.convert_to_pdf(filename)

    def convert_jpg_to_pdf(self, filename):
        return self.convert_to_pdf(filename)

    def convert_png_to_pdf(self, filename):
        return self.convert_to_pdf(filename)

    def convert_to_pdf(self, filename):
        filename_pdf = os.path.splitext(filename)[0] + ".pdf"
        outdir = os.path.split(filename_pdf)[0]
        ext = os.path.splitext(filename)[1]
        dest_file = filename.replace(ext, ".pdf")
        try:
            if self.args.verbose >= 3:
                print("convert ", filename, " ", dest_file)
            subprocess.check_call(["convert", filename, dest_file], stdout=subprocess.DEVNULL)
        except Exception as e:
            e.notification_message = _("An internal error occurred")
            raise e
        return filename_pdf

    def pdf_extract_images(self, filename_pdf):
        tmp_dir = tempfile._get_default_tempdir()
        tmp_basename = next(tempfile._get_candidate_names())
        tmp_basename_path = os.path.join(tmp_dir, tmp_basename)
        tmp_pdf_path = tmp_basename_path + ".pdf"
        self.temp_files.append(tmp_pdf_path)

        shutil.copy(filename_pdf, tmp_pdf_path)

        cmd_args = ["pdftoppm", "-png", tmp_pdf_path, tmp_basename_path]
        if self.args.verbose >= 3:
            print(" ".join(cmd_args))
        try:
            subprocess.check_call(cmd_args, stdout=subprocess.DEVNULL)
        except Exception as e:
            e.notification_message = _("An internal error occurred")
            raise e

        pdf_images = sorted(glob.glob(tmp_basename_path + "-*.png"))
        if len(pdf_images) == 1:
            self.temp_files.append(pdf_images[0])
            return pdf_images[0]
        else:
            input_files = []
            for img in pdf_images:
                input_files.append(img)
                self.temp_files.append(img)
            return input_files

    def convert_input(self):
        endlist = []
        if self.args.files == []:
            e = Exception(_("No input file."))
            e.notification_message = _("please provide an input file to process")
            raise e
        for fil in self.args.files:
            filename, ext = os.path.splitext(fil)
            ext = ext.lower()
            if ext in self.available_input_formats:
                endlist.append(fil)
                continue
            try:
                if ext == ".pdf":
                    convert_function = getattr(self, "pdf_extract_images")
                else:
                    convert_function = getattr(self, "convert_%s_to_jpg" %ext.replace(".",""))
                endlist.append(convert_function(fil))
            except Exception as e:
                e.notification_message = _("Couldn't manage the input format %s" %ext.replace(".","").lower())
                raise e
        self.input_files = endlist

    def convert_output(self):
        if '.' + self.args.output_ext not in self.available_output_formats:
            e = Exception(_("Output format %s is not supported" %self.args.output_ext))
            e.notification_message = _("Output format %s is not supported" %self.args.output_ext)
            raise e
        endlist = []
        for fil in self.output_files:
            filename, ext = os.path.splitext(fil)
            if ext.lower() == '.' + self.args.output_ext:
                endlist.append(fil)
                continue
            self.logger.info(_("Info: Converting output file %s to %s" %(fil, self.args.output_ext)))
            try:
                convert_function = getattr(self, "convert_%s_to_%s" %(ext.replace(".","").lower(), self.args.output_ext))
                endlist.append(convert_function(fil))
            except Exception as e:
                e.notification_message = _("Couldn't manage the output format %s" %ext.replace(".",""))
                raise e
        self.output_files = endlist


    def move_files_to_destination(self):
        dest_files = []
        for fil in self.output_files:
            ext = os.path.splitext(fil)[1].lower()
            dest = self.get_next_save_name() + ext
            shutil.move(fil, dest)
            dest_files.append(dest)
        self.output_files = dest_files

    def remove_tmp_files(self):
        if self.args.tmp_no_delete:
            return
        for fil in self.temp_files:
            os.remove(fil)

    def launch_libreoffice(self):
        if self.args.no_open:
            return
        self.send_notification(_("Opening libreoffice"))
        self.display = os.getenv("DISPLAY")
        if not self.args.batch_mode and self.display:
            subprocess.Popen(["soffice", self.output_files[0]], stdout=subprocess.DEVNULL)

    def ocrize(self):
        raise Exception('Not Implemented! Should be!')

    def launch(self):
        self.send_notification(_("Starting Optical Character Recognition"))
        self.logger.info(_('OCR Engine : ') + self.args.ocr_engine)
        try:
            self.scan()
            self.send_notification(_("Treating input image, please wait"))
            self.convert_input()
            self.send_notification(_("Recognizing text from image, please wait"))
            if not self.args.no_ocr:
                self.ocrize() # sets self.output_files
                self.send_notification(_("Finalising document, please wait"))
                self.convert_output()
            self.move_files_to_destination()
            self.remove_tmp_files()
            self.launch_libreoffice()
        except OcrizerException as e:
            self.send_notification(e.notification_message)
            sys.exit(1)
        except Exception as e:
            traceback.print_exc()
            self.logger.critical(e)
            self.send_notification(_("An error occurred during Optical Character Recognition"))
            sys.exit(1)
        self.logger.info(_("finished"))


# import available ocr Engines
from ocrize.ocrengines import *

def get_subclasses():
    engines = {}
    for classs in OCRizer.__subclasses__():
        engines[classs.engine_name] = classs
    return engines

# returns highest priority OCR engine name
def get_high_prio_engine(classes):
    try:
        highest_prio_eng = list(classes.keys())[0]
    except:
        print(_("Couldn't find any OCR engine available, exiting"))
        sys.exit(1)
    for eng_name, classs in classes.items():
        if classs.priority > classes[highest_prio_eng].priority:
            highest_prio_eng = eng_name
    return highest_prio_eng

def parse_args(argv=None):
    OCREngines_available = get_subclasses()
    if not len(OCREngines_available):
        print(_("Couldn't find any OCR engine available, exiting"))
        sys.exit(1)
    high_prio_engine = get_high_prio_engine(OCREngines_available)
    ocr_engines = OCREngines_available.keys()
    output_extensions = ["txt", "pdf", "odt"]

    parser = argparse.ArgumentParser(description=_('Mate-accessibility OCR tool'), epilog=_('The mate-accessibility tool for OCRising documents. includes a scanner function'), formatter_class=lambda prog: argparse.HelpFormatter(prog,max_help_position=5))
    parser.add_argument("files", action='append', type=str, nargs='*', metavar=_('file(s)'), default=None, help=_("file(s) to run OCR against"))
    parser.add_argument("-s", "--scan", action='store_true', default=False, dest="scan_document", help=_("scan mode. attempts to scan a file from available device. This option ignores other input files"))
    parser.add_argument("--scanner", action='store', type=str, default=None, dest="chosen_scanner", help=_("specify a scanner, understands sane-like devices, eg: 'genesys:libusb:001:002'"))
    parser.add_argument("--all-scanners", action='store_true', default=False, dest="all_scanners", help=_("attempt to scan from all scanners"))
    parser.add_argument("--list-scanners", action='store_true', default=False, dest="list_scanners", help=_("List all availables scanner"))
    parser.add_argument("-r", "--scan_resolution", action="store", type=int, default=300, dest="scan_resolution", help=_("scanner resolution"))
    parser.add_argument('-e', '--ocr-engine', action='store', choices=ocr_engines, type=str, default=high_prio_engine, dest="ocr_engine", help=_("The OCR engine to run"), metavar=_("engine"))
    parser.add_argument('-l', '--langage', action='store', type=str, default=os.getenv("LANG").split('.')[0], dest="language", help=_("the langage/locale to concentrate research on. eg: en_US"), metavar="lang")
    parser.add_argument("-o", "--output-format", action='store', type=str, default='odt', dest="output_ext", help=_("the desired output format"), metavar="format")
    parser.add_argument("-b", "--batch", action='store_true', default=False, dest="batch_mode", help=_("batch mode, do not open libreoffice in the end"))
    parser.add_argument("-v", "--verbose", action="store", type=int, default=4, dest="verbose", choices=range(6), help=_("verbosity level"))
    parser.add_argument("--no-syslog", action="store_false", default=True, dest="syslog", help=_("do not log to syslog"))
    parser.add_argument("--no-delete", action='store_true', default=False, dest="tmp_no_delete", help=_("do not delete temporary files (useful for debugging)"))
    parser.add_argument("--no-ocr", action='store_true', default=False, dest="no_ocr", help=_("do not delete use OCR engine (useful for debugging)"))
    parser.add_argument("--no-open", action='store_true', default=False, dest="no_open", help=_("do not open LibreOffice"))
    parser.add_argument("-d", "--destination", action="store", type=str, default=".", dest="output_dir", help=_("ocrized documents destination directory"))
    parser.add_argument("--no-finereader-post-processing", action='store_true', default=False, dest="finereader_no_postproc", help=_("do not post-process Finereader ODF files"))

    args = parser.parse_args(argv)
    args.ocr_engine_class = OCREngines_available[args.ocr_engine] # pass the class inside the args
    return args

class OcrizerException(Exception):
    pass

def start(argv=None):
    es = gettext.translation('ocrizer', fallback=True)
    es.install(names=['ngettext'])

    args = parse_args(argv)
    args.files = args.files[0]

    ocrizer = args.ocr_engine_class(args)
    ocrizer.launch()
