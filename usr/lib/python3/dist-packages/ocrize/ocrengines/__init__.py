import os
import inspect
#base_path = '/usr/lib/python3/dist-packages/ocrize/ocrengines'
base_path = os.path.dirname(inspect.getfile(inspect.currentframe()))
list_mods = [i[:-3] for i in os.listdir(base_path) if i.endswith(".py") and i != "__init__.py"]
__all__ = list_mods
