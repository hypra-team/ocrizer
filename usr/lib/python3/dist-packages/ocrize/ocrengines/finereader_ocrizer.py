#!/usr/bin/env python3

# Copyright 2017-2027 Auboyneau Vincent <ksamak@riseup.net>
# Copyright 2017 Hypra team <bugs@hypra.fr>
#
# This file is part of the OCRizer project.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

__author__ = "Ksamak"
__copyright__ = "Copyright 2017, Ksamak"
__credits__ = ["Hypra Sas"]
__license__ = "GPL"
__version__ = "3.0+"
__maintainer__ = "Alex ARNAUD"
__email__ = "bugs@hypra.fr"

from ocrize.ocrize import OCRizer

import os
import tempfile
import subprocess
from zipfile import ZipFile
from shutil import copyfileobj
from lxml import etree


class FinereaderOCRizer(OCRizer):
    engine_name = 'finereader'
    priority = 2
    def __init__(self, args):
        OCRizer.__init__(self, args)
        self.available_input_formats = [".tiff", ".png", ".jpeg", ".jpg", ".bmp", ".gif", ".dcx", ".jbig2"]
        self.available_output_formats = ['.txt', '.odt', '.htmlversion10defaults', '.htmlunicodedefaults', '.pdf', '.rtf', '.docx', '.xls', '.xlsx', '.xml', '.pptx', '.textversion10defaults', '.textunicodedefaults', '.alto', '.epub', '.fb2', '.odt']
        self.available_format_extentions = {'txt':'.txt', 'odt':'.odt', 'htmlversion10defaults':'.html', 'htmlunicodedefaults':'.html', 'pdf':'.pdf', 'rtf':'.rtf', 'docx':'.docx', 'xls':'.xls', 'xlsx':'.xlsx', 'xml':'.xml', 'pptx':'.pptx', 'textversion10defaults':'.txt', 'textunicodedefaults':'.txt', 'alto':'.alto', 'epub':'.epub', 'fb2':'.fb2'}
        self.lang_mapping = {'fr_FR':'French', 'en_GB':'English', 'en_US':'English', 'cs_CZ': 'Czech'}
        if not os.path.isfile('/usr/local/bin/abbyyocr11') or not os.access('/usr/local/bin/abbyyocr11', os.X_OK):
            e = Exception(_("abbyy Finereader executable '/usr/local/bin/abbyyocr11' not available."))
            e.notification_message = _("abbyy Finereader executable not found")

    def ocrize(self):
        if '.' + self.args.output_ext not in self.available_output_formats: # let the 'no available conversion func error play its role'
            target_format = 'textunicodedefaults'
        elif self.args.output_ext.replace('.' ,'').lower() == 'txt':
            target_format = 'textunicodedefaults'
        else:
            target_format = self.args.output_ext.replace('.' ,'')

        tmp_dir = tempfile._get_default_tempdir()
        for fil in self.input_files:
            tmp_name = next(tempfile._get_candidate_names()) + self.available_format_extentions[target_format]
            tmp_file = os.path.join(tmp_dir, tmp_name)
            self.logger.info("ORCizing file %s", fil)
            subprocess.check_call(['abbyyocr11', '-ido', '-rl', self.lang_mapping[self.args.language], '-if', fil, '-f', target_format, '-of', tmp_file], stdout=subprocess.DEVNULL)
            if target_format in ['odt'] and not self.args.finereader_no_postproc:
                postproc_tmp_file = tempfile.NamedTemporaryFile(delete=False, suffix=self.available_format_extentions[target_format])
                self._post_process_odf(tmp_file, postproc_tmp_file)
                postproc_tmp_file.close()
                self.temp_files.append(tmp_file)
                tmp_file = postproc_tmp_file.name
            self.output_files.append(tmp_file)

    def _get_pdf_page_count(self, filename_pdf):
        try:
            output = subprocess.check_output(["pdfinfo", filename_pdf])
            lines = output.decode('ASCII', errors='ignore').splitlines()
            pages_line = [line for line in lines if line.startswith("Pages:")][0]
            return int(pages_line.split(":")[1])
        except (FileNotFoundError, subprocess.CalledProcessError) as e:
            self.logger.warning(_('Failed to extract page count from PDF "{0}": {1}').format(filename_pdf, e))
            return 0

    def pdf_extract_images(self, filename_pdf):
        """ We pretend not to support PDF input because it doesn't properly
            handle pages not properly oriented.  To work around that, we
            convert to TIFF, which doesn't show the issue.
            However, TIFF files can easily grow huge, so we limit the
            conversion to smallish page count, and hope for the best with
            higher page count. """

        page_count = self._get_pdf_page_count(filename_pdf)
        self.logger.debug(ngettext('File "{0}" has one page',
                                   'File "{0}" has {1} pages', page_count).format(filename_pdf, page_count))
        if page_count < 1 or page_count >= 10:  # too many pages, just use the PDF itself and hope for the best
            self.logger.debug(_('Not converting PDF "{0}" to TIFF because it has too many pages').format(filename_pdf))
            return filename_pdf

        tiff_file = tempfile.NamedTemporaryFile(delete=False, suffix='.tiff').name
        self.temp_files.append(tiff_file)
        try:
            cmd = ['convert', '-density', '150', filename_pdf, '-background', 'white', '-alpha', 'remove', '-depth', '8', '-compress', 'lzw', tiff_file]
            self.logger.debug(_('Command for converting PDF to TIFF: {0}').format(cmd))
            subprocess.check_call(cmd, stdout=subprocess.DEVNULL)
        except (FileNotFoundError, subprocess.CalledProcessError) as e:
            self.logger.debug(str(e))
            self.logger.warning(_('Failed to convert PDF "{0}" to TIFF: falling back to using PDF directly'))
            return filename_pdf
        except Exception as e:
            e.notification_message = _("An internal error occurred")
            raise e
        return tiff_file

    def convert_output(self):
        if '.' + self.args.output_ext not in self.available_output_formats:
            e = Exception(_("Output format %s is not supported" %self.args.output_ext))
            e.notification_message = _("Output format %s is not supported" %self.args.output_ext)
            raise e
        endlist = []
        for fil in self.output_files:
            filename, ext = os.path.splitext(fil)
            if ext.lower() == self.available_format_extentions[self.args.output_ext]:
                endlist.append(fil)
                continue
            self.logger.info(_("Info: Converting output file %s to %s" %(fil, self.args.output_ext)))
            try:
                convert_function = getattr(self, "convert_%s_to_%s" %(ext.replace(".","").lower(), self.args.output_ext))
                endlist.append(convert_function(fil))
            except Exception as e:
                e.notification_message = _("Couldn't manage the output format %s" %ext.replace(".",""))
                raise e
        self.output_files = endlist

    def _post_process_odf(self, src, dst):
        def get_page_width(odf_zip, default='21cm'):
            try:
                styles_info = odf_zip.getinfo('styles.xml')
                with odf_zip.open(styles_info, 'r') as styles:
                    tree = etree.parse(styles)
                    nsmap = { 'fo':    'urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0',
                              'office':'urn:oasis:names:tc:opendocument:xmlns:office:1.0',
                              'style': 'urn:oasis:names:tc:opendocument:xmlns:style:1.0' }
                    return tree.xpath('//style:page-layout[@style:name=//office:master-styles/style:master-page/@style:page-layout-name]/style:page-layout-properties/@fo:page-width', namespaces=nsmap)[0]
            except KeyError:
                return default

        xsl_path = os.path.join(self.engine_datadir, 'odf-post-process.xsl')
        with open(xsl_path, 'rb') as xsl:
            stylesheet = etree.XSLT(etree.parse(xsl))

            with ZipFile(src, 'r') as odf_in:
                with ZipFile(dst, 'w') as odf_out:
                    for info in odf_in.infolist():
                        with odf_in.open(info, 'r') as i:
                            if info.filename == 'content.xml':
                                parameters = {
                                    'page-width': etree.XSLT.strparam(get_page_width(odf_in))
                                }
                                data = bytes(stylesheet(etree.parse(i), **parameters))
                                for e in stylesheet.error_log:
                                    self.logger.debug('%s:%s:%s: %s' % (os.path.basename(xsl_path), e.line, e.column, e.message))
                                odf_out.writestr(info, data)
                            else:
                                odf_out.writestr(info, i.read())
