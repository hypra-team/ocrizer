#!/usr/bin/env python3

# Copyright 2017-2027 Auboyneau Vincent <ksamak@riseup.net>
# Copyright 2017 Hypra team <bugs@hypra.fr>
#
# This file is part of the OCRizer project.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

__author__ = "Ksamak"
__copyright__ = "Copyright 2017, Ksamak"
__credits__ = ["Hypra Sas"]
__license__ = "GPL"
__version__ = "3.0+"
__maintainer__ = "Alex ARNAUD"
__email__ = "bugs@hypra.fr"

from ocrize.ocrize import OCRizer
import os
import tempfile
import subprocess

class TesseractOCRizer(OCRizer):
    engine_name = 'tesseract'
    priority = 1
    def __init__(self, args):
        OCRizer.__init__(self, args)
        self.available_input_formats = [".png", ".jpeg", ".jpg"]
        self.available_output_formats = [".txt", ".odt"]
        self.lang_mapping = {'fr_FR':'fra', 'en_GB':'eng', 'en_US':'eng'}

    def convert_images_to_text(self, input_files):
        for fil in input_files:
            if type(fil) is list:
                if self.args.verbose >= 5:
                    print('find an input file as list : %s', fil)
                self.convert_images_to_text(fil)
            else:
                if self.args.verbose >= 5:
                    print(fil, " stdout ", "-l " + self.lang_mapping[self.args.language])
                self.ocrized_text += subprocess.check_output(['tesseract', fil, "stdout", "-l", self.lang_mapping[self.args.language],  "-psm", "1"]).decode("utf-8")
                self.ocrized_text += "\n"

    def ocrize(self):
        self.convert_images_to_text(self.input_files)

        tmp_dir = tempfile._get_default_tempdir()
        tmp_name = next(tempfile._get_candidate_names()) + '.txt'
        tmp_file = os.path.join(tmp_dir, tmp_name)
        with open(tmp_file, "w") as out_file:
            out_file.write(self.ocrized_text)
        self.output_files.append(tmp_file)
