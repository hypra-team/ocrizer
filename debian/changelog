ocrizer (1.1.2) stretch; urgency=medium

  * Add Czech language support to Finereader backend

 -- Alex ARNAUD <aarnaud@hypra.fr>  Mon, 21 Oct 2019 14:38:27 +0200

ocrizer (1.1) stretch; urgency=medium

  * Remove ksamak from the uploaders
  * change Alex ARNAUD mail to aarnaud@hypra.fr
  * add the support for .PDF extension (in uppercase)

 -- Alex ARNAUD <aarnaud@hypra.fr>  Thu, 12 Sep 2019 16:06:10 +0200

ocrizer (1.0.1+deb9) stretch; urgency=medium

  * ocrizer.py: create the class OcrizerException to report exceptions
    to users
  * ocrizer.py: report to the user when no device is found to avoid him
    to wait indefinitely
  * When no scanner is found a simpler error is now reported to the
    user, we report now "Couldn't find an obvious scanner device"

 -- Alex ARNAUD <alexarnaud@hypra.fr>  Thu, 09 May 2019 16:19:33 +0200

ocrizer (1.0.0) jessie; urgency=medium

  [ Colomban Wendling ]
  * ocrize.py: fix error recovery when converting a PDF to an image fails.
  * ocrengines/finereader_ocrizer.py: add post-processing to make the
    output ODF files more readable with Orca.
  * Fix converting TXT to ODT when LibreOffice is already running.
    This fixes the Tesseract backend with the default option when
    LibreOffice is already running.
  * ocrengines/finereader_ocrizer.py: Fix handling of improperly oriented
    PDFs.
  * po/fr.po: Update French translation.

  [ Alex ARNAUD ]
  * ocrize.py: display OCR engine on each call on the console, not only
    when scanning a document
  * ocrize.py: display an error to the user when it occurs to avoid
    waiting indefinitely
  * Make USB scanners have a priority over network ones

 -- Alex ARNAUD <alexarnaud@hypra.fr>  Tue, 09 Apr 2019 12:38:20 +0200

ocrizer (0.4.1) stretch; urgency=medium

  [ Alex ARNAUD ]
  * change the default desktop icon from the Mate generic one to printer
  * Add the category Graphics to the desktop icon to make the software
    discoverable on the Mate menu

 -- Cyril Brulebois <cyril@debamax.com>  Fri, 20 Apr 2018 01:45:41 +0200

ocrizer (0.4) jessie; urgency=medium

  [ Alex ARNAUD ]
  * when a chosen scanner is given and not found it is now reported to
    the user
  * update french translation to fix a spelling mistake
  * add support for multipage PDF with the tesseract backend
  * update copyright header to include Hypra inside the copyright and
    to update the maintainer field
  * add poppler-utils as dependency because pdftoppm is used inside the
    package
  * ocrize.py: prioritize LibreOffice 4.2 over LibreOffice 4.3 or 5.x
    to avoid issues with newer LibreOffice releases (e.g. stop reading
    text)
  * ocrizer: launch libreoffice in a separate proccess to avoid
    freezing Caja until libreoffice is closed
  * add a sanity check on the mode definition, if the driver doesn't
    allow the mode color (for exemple Samsung) the default mode will be
    used and the execution will continue
  * change the way of detecting scanners: exclude webcams because they
    are seen by sane as "virtual device" whereas there are multiple ways
    of naming scanner depending of driver: scanner, all-in-one,
    multifunction.
  * ocrize.py: in the select_scanner function, leave the for loop when a
    scanner is found.

 -- Cyril Brulebois <cyril@debamax.com>  Tue, 09 Jan 2018 01:38:54 +0100

ocrizer (0.3.1) stretch; urgency=medium

  * Rebuild within stretch.

 -- Cyril Brulebois <cyril@debamax.com>  Thu, 31 Aug 2017 22:55:21 +0200

ocrizer (0.3) jessie; urgency=medium

  [ Alex ARNAUD ]
  * fix package-depends-on-multiple-libpng-versions
  * add libpng16-16 as dependency to be compatible with Debian Stretch
  * fix NameError exception due to a wrong scope of the 'args' variable
  * finereader_ocrizer.py: overrride the convert_output function to
    ensure that the output formats that are differents of the extension
    could be supported. For exemple, with the finereader backend they
    are two output formats for HTML with the .html extension. We have a
    mapping array that rely an output format to an extension, it is now
    used. THis avoid to try to convert from html to
    htmlversion10defaults. .htmlversion10defaults is the abby finereader
    specific output format so if the file is already in HTML we no
    longer need to convert it. In the earlier release if we try to use a
    output format different of the extension that produced always an
    error.
  * debian/rules: overrride dh_clean to remove the file po/ocrizer.mo
    generated on the dh_auto_configure stage

 -- Cyril Brulebois <cyril@debamax.com>  Thu, 29 Jun 2017 10:53:07 +0200

ocrizer (0.2.3+jessie1) jessie; urgency=medium

  * Jessie release.

 -- Cyril Brulebois <cyril@debamax.com>  Tue, 04 Apr 2017 10:23:37 +0200

ocrizer (0.2.3) unstable; urgency=medium

  [ Alex ARNAUD ]
  * ocrengines/tesseract_ocrizer.py: adding "psm" argument to the teseract
    command to detect page orientation
  * ocrize.py: add the support for ~ on beginning of file path

 -- Cyril Brulebois <cyril@debamax.com>  Tue, 04 Apr 2017 10:16:18 +0200

ocrizer (0.2.2+jessie1) jessie; urgency=medium

  * Jessie release.

 -- Cyril Brulebois <kibi@debian.org>  Wed, 22 Feb 2017 04:48:16 +0100

ocrizer (0.2.2) unstable; urgency=medium

  [ Alex ARNAUD ]
  * debian/control: fix file conflict with mate-accessibility-ocr-
    integration-caja prior to version 0.7  on
    /etc/skel/Desktop/ocrizer.desktop

 -- Cyril Brulebois <cyril@debamax.com>  Mon, 30 Jan 2017 17:12:51 +0100

ocrizer (0.2.1) unstable; urgency=medium

  [ Alex ARNAUD ]
  * ocrize.py: in function "get_max_resolution" sorts the value inside the
    array to be sure that the last value is always the max value
  * ocrize.py: add the OCR Engine to the logs (info level)
  * ocrize.py: add the resolution to the logs (info level)
  * ocrize.py: treat the resolution parameter only if it is available
    (with Webcams for exemple)
  * ocrize.py: fix broken automated scanner selection due to a  missing
    "return" in the function "select_scanner"

 -- Cyril Brulebois <kibi@debian.org>  Sun, 18 Dec 2016 01:18:42 +0100

ocrizer (0.2) unstable; urgency=medium

  [ Alex ARNAUD ]
  * update the desktop icon to be compatible with all French languages
  * update the desktop icon permissions because it is not an executable file
  * ocrizer.desktop: move it from ocrizer to ocrizer-common package
  * debian/copyright: update it to have the corrects information and makes it
    compatible with the Debian policy
  * debian/control: update "Maintainer" field
  * debian/control: add "Uploaders:" field
  * debian/control: Move "Standards-Version" from 3.9.3 to 3.9.6
  * debian/control: Add "${misc:Depends}" as dependency on all packages
  * debian/control: Add python3 as dependency on ocrizer, ocrizer-common
    and ocrizer-finereader packages
  * debian/control: update packages descriptions
  * ocrize.py: Add controls functions to block the scan resolution to the
    max value
  * ocrize.py: change scan default resolution from 450 to 300
  * ocrize.py: bugfix: never treat hidden files in folder parsing that throws
    an python exception.
  * ocrize.py: move scanner selection code to a specific function called
    "select_scanner"
  * ocrize.py: add "--list-scanners" for listing available scanners
  * ocrize.py: print the chosen scanner with the logger

 -- Cyril Brulebois <kibi@debian.org>  Sat, 26 Nov 2016 00:44:27 +0100

ocrizer (0.1.2+sec1) jessie; urgency=medium

  * put higher priority to LO4.2

 -- ksamak <ksamak@riseup.net>  Tue, 07 Jun 2016 12:57:47 +0200

ocrizer (0.1.2) jessie; urgency=low

  * trust sane for bit depth
  * added compat for LO4.2

 -- ksamak <ksamak@riseup.net>  Tue, 31 May 2016 16:13:16 +0200

ocrizer (0.1.1) jessie; urgency=low

  * added resolution parameter

 -- ksamak <ksamak@riseup.net>  Mon, 23 May 2016 20:18:28 +0200

ocrizer (0.1) jessie; urgency=low

  * Initial release.

 -- ksamak <ksamak@riseup.net>  Thu, 12 May 2016 22:31:07 +0200
